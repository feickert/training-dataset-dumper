#
# Build configuration for the IOTests package
#

# Set the name of the package:
atlas_subdir( BTagTrainingPreprocessing )

# Packages that this package depends on:
atlas_depends_on_subdirs(
  PRIVATE
  Control/xAODRootAccess
  Event/xAOD/xAODEventInfo
  Event/xAOD/xAODCaloCluster
  Event/xAOD/xAODTrack
  Event/xAOD/xAODTruth
  Event/xAOD/xAODCutFlow
  Event/xAOD/xAODPFlow
  PhysicsAnalysis/AnalysisCommon/HDF5Utils
  PhysicsAnalysis/JetTagging/FlavorTagDiscriminants
  Control/AthToolSupport/AsgTools
  Reconstruction/Jet/JetCalibTools
  Reconstruction/Jet/JetSelectorTools
  Reconstruction/Jet/BoostedJetTaggers
  InnerDetector/InDetRecTools/InDetTrackSelectionTool
  Reconstruction/Jet/JetMomentTools
  Tools/PathResolver
)

# External(s) used by the package:
find_package(HDF5 1.10.1 REQUIRED COMPONENTS CXX C)

# common requirements
add_library(dataset-dumper
  src/BTagJetWriter.cxx
  src/BTagJetWriterConfig.cxx
  src/BTagTrackWriter.cxx
  src/JetConstWriter.cxx
  src/BookKeeper.cxx
  src/addMetadata.cxx
  src/TrackSelector.cxx
  src/TruthTools.cxx
  src/VRJetOverlapCandidates.cxx
  src/ConstituentSelector.cxx
  src/BrokenEventIndex.cxx
  src/ConfigFileTools.cxx)
target_include_directories(dataset-dumper PRIVATE src)
target_link_libraries(dataset-dumper
  ${HDF5_LIBRARIES}
  xAODRootAccess
  xAODCaloEvent
  xAODTracking
  xAODPFlow
  xAODJet
  xAODTruth
  HDF5Utils
  xAODCutFlow
  AsgTools
  JetCalibToolsLib
  JetSelectorToolsLib
  InDetTrackSelectionToolLib
  JetMomentToolsLib
  BoostedJetTaggersLib
  FlavorTagDiscriminants
  PathResolver
  )

# Build the test executables
atlas_add_executable( dump-single-btag
  util/dump-single-btag.cxx util/SingleBTagOptions.cxx util/SingleBTagConfig.cxx)
target_link_libraries( dump-single-btag dataset-dumper)
target_include_directories( dump-single-btag PRIVATE src)

atlas_add_executable( dump-hbb
  util/dump-hbb.cxx util/HbbOptions.cxx util/HbbConfig.cxx)
target_link_libraries( dump-hbb dataset-dumper)
target_include_directories( dump-hbb PRIVATE src)

atlas_add_executable( test-btagging util/test-btagging.cxx )
target_link_libraries( test-btagging dataset-dumper)
target_include_directories( test-btagging PRIVATE src)
