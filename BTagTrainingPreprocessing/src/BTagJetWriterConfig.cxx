#include "BTagJetWriterConfig.hh"

BTagJetWriterConfig::BTagJetWriterConfig():
  name(""),
  write_event_info(false),
  write_kinematics_relative_to_parent(false),
  write_substructure_moments(false)
{
}
