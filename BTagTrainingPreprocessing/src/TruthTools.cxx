#include "TruthTools.hh"

bool TruthTools::is_from_W(const xAOD::TruthParticle &truth_particle) {
  for (std::size_t parent_index = 0; parent_index < truth_particle.nParents(); parent_index++) {
    const xAOD::TruthParticle &parent = *truth_particle.parent(parent_index);
    if (parent.isW() || is_from_W(parent)) return true;
  }
  return false;
}

bool TruthTools::is_overlaping_lepton(const xAOD::Jet &jet, const xAOD::TruthParticleContainer &truth_particles, float dR) {
  for (const xAOD::TruthParticle* truth_particle: truth_particles) {

    // protection for slimmed event record
    if ( ! truth_particle) continue;

    // now check if it's an overlaping election or muon
    if (! (truth_particle->isElectron() || truth_particle->isMuon()) ) continue;
    if (truth_particle->pt() < 10000 || truth_particle->status() != 1) continue;
    if (jet.p4().DeltaR(truth_particle->p4()) > dR) continue;

    // ...from a W
    if (is_from_W(*truth_particle)) return true;
  } // end of particle loop
  return false;
}
