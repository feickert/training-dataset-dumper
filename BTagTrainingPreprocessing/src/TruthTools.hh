#ifndef TRUTH_TOOLS_HH
#define TRUTH_TOOLS_HH

#include "xAODJet/JetContainer.h"
#include "xAODTruth/TruthParticleContainer.h"


class TruthTools
{
public:
  bool is_from_W(const xAOD::TruthParticle &truth_particle);
  bool is_overlaping_lepton(const xAOD::Jet &jet, const xAOD::TruthParticleContainer &truth_particles, float dR);
};

#endif
