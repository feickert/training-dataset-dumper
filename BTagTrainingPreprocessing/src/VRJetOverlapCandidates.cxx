#include "VRJetOverlapCandidates.hh"

namespace {
  double getRadius(const TLorentzVector& vec) {
    return std::max(0.02, std::min(0.4, 30e3 / vec.Pt()));
  }
}

VRJetOverlapCandidates::VRJetOverlapCandidates(const xAOD::JetContainer& jets,
                                               VRJetSelection sel)
{
  for (const auto& jet: jets) {
    if (sel == VRJetSelection::TWIKI) {
      if (jet->pt() < 5e3) continue;
      if (jet->numConstituents() < 2) continue;
    }
    TLorentzVector p4 = jet->p4();
    m_jets.emplace_back(p4);
  }
}

double VRJetOverlapCandidates::relativeDeltaR(const xAOD::Jet& jet) const {
  double closest_jet_relative_dr = INFINITY;
  TLorentzVector p4 = jet.p4();
  double r = getRadius(p4);
  for (const auto& orjet: m_jets) {
    double dR = orjet.p4.DeltaR(p4);
    // ignore if it's the same jet
    if (dR < 1e-5) continue;
    double min_radius = std::min(orjet.radius, r);
    double rel_radius = dR / min_radius;
    closest_jet_relative_dr = std::min(rel_radius, closest_jet_relative_dr);
  }
  return closest_jet_relative_dr;
}

detail::ORJet::ORJet(const TLorentzVector& _p4):
  p4(_p4),
  radius(getRadius(_p4))
{}
